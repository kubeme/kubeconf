package kubeconf

import (
	"fmt"
	"os"
	"path/filepath"
	"gopkg.in/yaml.v2"
)
type KubeConfig struct {
	Clusters []struct {
		Name    string `yaml:"name"`
		Cluster struct {
			Server string `yaml:"server"`
		} `yaml:"cluster"`
	} `yaml:"clusters"`
	Contexts []struct {
		Name    string `yaml:"name"`
		Context struct {
			Cluster string `yaml:"cluster"`
			User    string `yaml:"user"`
		} `yaml:"context"`
	} `yaml:"contexts"`
	CurrentContext string `yaml:"current-context"`
}

func GetConfigPaths(defaultKubeConfigPath string) []string {
	var kubeConfigPaths []string
	if path, exists := os.LookupEnv("KUBECONFIG"); exists {
		// Wenn die Umgebungsvariable KUBECONFIG gesetzt ist, kann sie mehrere Pfade enthalten.
		kubeConfigPaths = append(kubeConfigPaths, filepath.SplitList(path)...)
		fmt.Println("KUBECONFIG: ", path)
	} else {
		// Standardpfad hinzufügen, falls keine Umgebungsvariable KUBECONFIG vorhanden ist.
		kubeConfigPaths = append(kubeConfigPaths, defaultKubeConfigPath)
		fmt.Println("KUBECONFIG ist nicht gesetzt. Verwende den Standardpfad:", defaultKubeConfigPath)
	}
	return kubeConfigPaths
}

// Überprüft, ob eine kubeconfig-Datei im angegebenen Pfad existiert.
func CheckKubeConfigPath(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func GetKubeConfigs(kubeConfigPaths []string) map[string]KubeConfig {
	kubeConfigs := make(map[string]KubeConfig)
	for _, path := range kubeConfigPaths {
		if CheckKubeConfigPath(path) {
			kubeConfig, err := ReadKubeConfig(path)
			if err == nil {
				kubeConfigs[path] = kubeConfig
			} else {
				fmt.Printf("Fehler beim Lesen der Datei: %v\n", err)
			}
		} else {
			fmt.Printf("Nicht gefunden: %s\n", path)
		}
	}
	return kubeConfigs
}



// Liest und gibt den Inhalt der kubeconfig-Datei aus.
func ReadKubeConfig(path string) (KubeConfig, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return KubeConfig{}, err
	}

	var kubeConfig KubeConfig
	err = yaml.Unmarshal(content, &kubeConfig)
	if err != nil {
		return KubeConfig{}, err
	}

	return kubeConfig, nil
}
